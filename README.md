# RNAseqFastqToReadCounts

Snakemake orchestrated containerised pipeline to process RNAseq data from raw fastq reads to read counts. The obtained read counts can be further used in downstream analyses implemented in the EpiGen's follow up [RNAseq pipeline](https://gitlab.com/Ylefol/rnaseq_pipeline_yl) or can be combined with ATACseq data using the [FeralFleaPainer](https://gitlab.com/epigen/feralfleapainter).

## Table of Contents
1. [Overview](#overview)
2. [Pipeline Setup](#pipeline-setup)
3. [Pipeline Usage](#pipeline-usage)
4. [Folder Structure And Contents Of The Results](#folder-structure-and-contents-of-the-results)
5. [Limitations](#limitations)
6. [Questions, Comments, Suggestions](#questions-comments-suggestions)
7. [ToDo](#todo)

## Overview

This pipeline processes raw RNAseq fastq reads to the form of a read counts table. It checks and visualises quality of raw sequencing data using `fastqc` and `multiqc`, it trimms the raw data with `trimmomatic` according to user specified requirements and checks quality of the trimmed data with `fastqc` and `multiqc` again. Read mapping against the specified reference genome is performed with `STAR` and further lane merging and read filtering is done with `samtools`. Transformation of the postprocessed mapping files to bigwig file format (for IGV visualisation and possible export from TSD) is done with `deeptools` command `bamCoverage`. The `subread`'s command `featureCounts` is used to perform read counting according to options specified in the config file. The read counts are joined together according to the groups specified in the `group` column of the file info input file and also a table of read counts of all the analysed samples is created. At the end, `multiqc` collects log files from all the used tools and creates a summary report about the data processing.

All the bioinformatics tools are wrapped in a singularity container. For more details about the tools' versions and container itself, see [containers/README.md](https://gitlab.com/epigen/rnaseqfastqtoreadcounts/-/blob/main/containers/README.md).


In the following figure, all the `snakemake` rules (code components) of the pipeline are shown. The rules are organised in such a way that the high-level rules - the rules which are called when running the pipeline - are shown at the bottom of the figure. The code components are run in groups, when a high-level rule is called, all the rules of the same color will be run. When there is an arrow between two code components, results of the source component have to be available before the destination code component is executed. It means that all the code components to the left from a called high-level rule have to be done before the rule starts to be executed - this can happen in two ways, either by running the high-level rules consecutively from left to right and inspecting intermediate results of the pipeline or by executing a high-level rule in which case all the missing bits of the pipeline will be run automaticaly. The arrows starting in the `check_file_sample_lane_presence` and crossing between different rule groups are dashed for better readability. When a conventional bioinformatics tool is used in a snakemake rule without mentioning the tool in the rule name, the tool is listed below the rule.

![Snakemake rulegraph](imgs/rulegraph.png "Snakemake rulegraph")


### Dependencies

The pipeline uses `snakemake` for workflow management and thus also depends on `python3` and `pandas`. It needs `singularity` to be installed as well. The versions used in our TSD setup are listed here:


<center>

| Software | Used Version |
| :--- | :---: |
| `python3` | 3.9.5 |
| `snakemake` | 6.6.1 |
| `singularity` | 3.7.3 |

</center>


## Pipeline Setup


### Initialise dependencies in TSD before executing the pipeline 

Before executing the pipeline on the submit node, run the following commands to make all the necessary software tools available in the current terminal window:

```
module purge
module load Python/3.9.5-GCCcore-10.3.0
module load singularity/3.7.3
module load snakemake/6.6.1-foss-2021a
```

### Other TSD specifics

All the pipeline files, containers, data resources and all the other files used by the pipeline have to be on the /cluster partition.

### General setup

1. Clone/download/transfer content of a tagged version of this repository to the computing infrastructure where this pipeline will be used.
2. Download and collect all the necessary data resources: reference genome, gtf annotation of the reference genome, `fastqc` and `trimmomatic` specific files for adapter/contaminant removal should the files be used.
3. Create a file containing info about all the files included in the analysis and in the next step set the file's path to be the value of the `INPUT_FASTQ_INFO_SHEET` variable in `pipeline_config.yaml`. Check `file_info_example.tsv` for an example of such a file. The file have to contain tab-separated values with the first line of the file being its header and consisting of the following column names: `file_name`, `sample_name`, `base_name`, `lane`, `read`, `group`, where the columns' content is: 

      - `file_name` is full file name without it's path,
      - `sample_name` is the prefix of the file name preceding `_lane_read_001.fastq.gz`,
      - `base_name` is the prefix of the file name preceding `.fastq.gz`,
      - `lane` is lane ID starting with letter L followed by three digits,
      - `read` is read ID, either R1 or R2,
      - `group` specifies groups according to which the read counts of different samples will be separated into the merged read count files,
      - `instrument_id` is the ID of the sequencing instrument on which the sample was sequenced - can be obtained from the `fastq` file
      - `flowcell_id` is the ID of the flowcell on which the sample was sequenced - can be obtained from the `fastq` file

	There are two helping scripts called `create_links.sh` and `generate_file_info.sh` in the `scripts` subfolder which are meant to help to create such `file_info.tsv` file:

      a. Script `create_links.sh` deals with a situation when `fastq.gz` files are separated into different subfolders according to the sample nama as this is how we usually get the data. The script creates one additional subfolder of the input folder and it will contain links to all the `fastq.gz` files for easier accessability. Processing with this script can be skipped if you have all the `fastq` files located in one folder. Usage is as follows:


	```
	bash scripts/create_links.sh <PATH_TO_FOLDER_WITH_FASTQ_FILES_IN_LEVEL_1_SUBFOLDERS> \
		&> create_links.log
	```


      b. Script `generate_file_info.sh` iterates through all the `fastq.gz` files in an input folder and generates the `file_info.tsv` file based on the file names and content of the files. Usage follows:

	```
	bash scripts/generate_file_info.sh <PATH_TO_FOLDER_CONTAINING_FASTQ_FILES> \
		> file_info.tsv \
		2> generate_file_info.log
	```

4. Edit the `pipeline_config.yaml` file to contain information specific for the project at hand.
5. If slurm is the server's workload manager, `slurm_config.yaml` can be used to specify job requirements (this is true for TSD). Adjust the account id in the `__default__` rule to correspond to the used account. In the commands of the following section, slurm is assumed to be the workload manager.



## Pipeline Usage

The pipeline is divided into five different steps where each of them depends on the previous ones being executed successfully. Using this setup, the intermediate results can be checked and parameters for the following step can be chosen after the data checks. However, the whole pipeline can be run in one go when the last step of the sequence (`final_multiqc`) is executed without running the previous ones first (increase value of the `--cores` option to `16` when doing so). This can be useful when the whole pipeline needs to be re-run from scratch, but the user is sure that all the parameters are set up properly.

The following commands are meant to be run in a terminal window from inside of the folder containing this pipeline. To check the progress of the processing steps, one can inspect the `snakemake_*.std*` files and check the job queue status. All the outputs of the executed analysis will be stored in the `ANALYSIS_FOLDER` or its subfolders. The `ANALYSIS_FOLDER` is defined in the `pipeline_config.yaml` config file and should not change during the analysis.


1. Initialisation step:

	```
	NOW=$(date +%Y-%m-%d_%H%M%S)
	
	snakemake -p --configfile pipeline_config.yaml --cores 1 init \
		>> snakemake_init_${NOW}.stdout \
		2>> snakemake_init_${NOW}.stderr
	```

	Output data of this step will be stored in the subfolder called `00_file_sample_lane_presence`. It takes only a couple of seconds to run this step, no data analysis is done yet, but the files necessary for the further processing are generated. Proceed to the next step of the analysis only if the files `00_params.done` and `00_file_sample_lane_presence.done` are present in the `ANALYSIS_FOLDER`. Otherwise, check the content of the generated `snakemake_init*` files to narrow down the existing issue, fix it and re-run this step of the analysis. 


2. Perform quality control of the raw sequencing data:

	Before running the following commands, make sure that the adapters and possible contaminants of the experiment are included in the files specified in `pipeline_config.yaml` used by `fastqc`. If the options are left empty, the default `fastqc` files of adapters and contaminants will be used here.

	```
	# dry run: run it to see which commands will be executed without executing them, good to catch obvious bugs
	snakemake -np --configfile pipeline_config.yaml --cores 1 raw_data_qc
	
	# running raw data qc for real
	NOW=$(date +%Y-%m-%d_%H%M%S)
	
	snakemake -p --verbose \
		--reason \
		--cores 10 \
		--latency-wait 50 \
		--configfile pipeline_config.yaml \
		--cluster-config slurm_config.yaml \
		--cluster \
		  "sbatch -A {cluster.account} -c {cluster.c} -t {cluster.time} --mem-per-cpu {cluster.mem-per-cpu} --output={cluster.output} --error={cluster.error} --job-name={cluster.job-name}" \
		raw_data_qc \
		>> snakemake_raw_data_qc_${NOW}.stdout \
		2>> snakemake_raw_data_qc_${NOW}.stderr
	```

	Output of this step includes `fastqc` reports for each of the raw sequencing `fastq.gz` files and a summarising `multiqc` report for all the `fastq.gz` files together. The reports are stored in the `01_raw_data_qc/fastqc` and `01_raw_data_qc/multiqc` subfolders. Check the reports and proceed to the next step of the analysis only if the files `01_params_fastqc.done` and `01_raw_data_qc.done` are present in the `ANALYSIS_FOLDER`. Otherwise check content of the generated `snakemake_raw_data_qc*` files and the log files located in the `logs/01_raw_data_qc` subfolder of the `ANALYSIS_FOLDER` to narrow down the existing issue, fix it and re-run this step of the analysis. 


3. Perform data trimming and qc the trimmed data:

	Make sure that `trimmomatic` parameters are set up as required in the `pipeline_config.yaml` file.
	
	```
	# dry run
	snakemake -np --configfile pipeline_config.yaml --cores 1 trim_raw_data_and_qc_trimmed
	
	# running data trimming and additional qc for real
	NOW=$(date +%Y-%m-%d_%H%M%S)
	
	snakemake -p --verbose \
		--reason \
		--cores 10 \
		--latency-wait 50 \
		--configfile pipeline_config.yaml \
		--cluster-config slurm_config.yaml \
		--cluster \
		  "sbatch -A {cluster.account} -c {cluster.c} -t {cluster.time} --mem-per-cpu {cluster.mem-per-cpu} --output={cluster.output} --error={cluster.error} --job-name={cluster.job-name}" \
		trim_raw_data_and_qc_trimmed \
		>> snakemake_trim_raw_data_and_qc_trimmed_${NOW}.stdout \
		2>> snakemake_trim_raw_data_and_qc_trimmed_${NOW}.stderr
	```

	Results of this step are stored in the `02_trimmed_data/fastq`, `02_trimmed_data/fastqc` and `02_trimmed_data/multiqc` subfolders. Check the results and proceed to the next step of the analysis only if the files `02_params_trimmomatic.done` and `02_trim_raw_data_and_qc_trimmed.done` are present in the `ANALYSIS_FOLDER`. Otherwise check content of the generated `snakemake_trim_raw_data_and_qc_trimmed*` files and the log files in the `logs/02_trimmed_data` to narrow down the existing issue, fix it and re-run this step of the analysis.

4. Map the trimmed reads to the reference genome:

	Make sure that all the mapping parameters in the `pipeline_config.yaml` file are set up as required.

	```
	# dry run
	snakemake -np --configfile pipeline_config.yaml --cores 1 map_to_reference
	
	# running the mapping for real
	NOW=$(date +%Y-%m-%d_%H%M%S)
	
	snakemake -p --verbose \
		--reason \
		--cores 16 \
		--latency-wait 50 \
		--configfile pipeline_config.yaml \
		--cluster-config slurm_config.yaml \
		--cluster \
		  "sbatch -A {cluster.account} -c {cluster.c} -t {cluster.time} --mem-per-cpu {cluster.mem-per-cpu} --output={cluster.output} --error={cluster.error} --job-name={cluster.job-name}" \
		map_to_reference \
		>> snakemake_map_to_reference_${NOW}.stdout \
		2>> snakemake_map_to_reference_${NOW}.stderr
	```

	Results of this step are stored in the `03_mapping_to_ref_genome` subfolder and are separated into three subfolders called `bigwigs` (containing all bigwig files), `mapped_samples` (containing all bam files and some additional outputs) and `ref_genome_index` (with the reference genome index generated with `STAR`). Check the results and proceed to the next step of the analysis only if the files `03_params_mapping.done` and `03_map_to_reference.done` are present in the `ANALYSIS_FOLDER`. Otherwise check content of the generated `snakemake_map_to_reference*` files and the log files in the `logs/03_mapping_to_ref_genome` subfolder of the `ANALYSIS_FOLDER` to narrow down the existing issue, fix it and re-run this step of the analysis.

5. Count the mapped reads:

	Make sure that the `featureCounts` additional parameters are set up as required in the `pipeline_config.yaml` file.

	```
	# dry run
	snakemake -np --configfile pipeline_config.yaml --cores 1 count_reads

	# running the read counting for real
	NOW=$(date +%Y-%m-%d_%H%M%S)

	snakemake -p --verbose \
		--reason \
		--cores 16 \
		--latency-wait 50 \
		--configfile pipeline_config.yaml \
		--cluster-config slurm_config.yaml \
		--cluster \
		  "sbatch -A {cluster.account} -c {cluster.c} -t {cluster.time} --mem-per-cpu {cluster.mem-per-cpu} --output={cluster.output} --error={cluster.error} --job-name={cluster.job-name}" \
		count_reads \
		>> snakemake_count_reads_${NOW}.stdout \
		2>> snakemake_count_reads_${NOW}.stderr
	```

	Output files of this step are located in the `04_read_counts` subfolder and are separated into two folders called `separately` (containing read count files for each of the analysed samples) and `joined` (with the read count files joined together (a) by the groups defined in the file info file and (b) read counts of all the analysed samples joined into one table). Check the output files and proceed to the next step of the analysis only if the files `04_params_readcount.done` and `04_count_reads.done` are present. Otherwise check content of the generated `snakemake_count_reads*` files and the log files in the `logs/04_read_counts` subfolder of the `ANALYSIS_FOLDER` to narrow down the existing issue, fix it and re-run this step of the analysis.


6. Final run of multiqc gathering all the info from all the subfolders of `ANALYSIS_FOLDER`

	```
	# dry run
	snakemake -np --configfile pipeline_config.yaml --cores 1 final_multiqc
	
	# run the final multiqc
	NOW=$(date +%Y-%m-%d_%H%M%S)
	
	snakemake -p --verbose \
		--reason \
		--cores 1 \
		--latency-wait 50 \
		--configfile pipeline_config.yaml \
		--cluster-config slurm_config.yaml \
		--cluster \
		  "sbatch -A {cluster.account} -c {cluster.c} -t {cluster.time} --mem-per-cpu {cluster.mem-per-cpu} --output={cluster.output} --error={cluster.error} --job-name={cluster.job-name}" \
		final_multiqc \
		>> snakemake_final_multiqc_${NOW}.stdout \
		2>> snakemake_final_multiqc_${NOW}.stderr
	```

	Results of this step are located in the `05_multiqc` subfolder. If the files `05_params_multiqc.done` and `05_multiqc.done` are present after executing the command above, the RNAseq analysis successfully finished.


## Folder Structure And Contents Of The Results

Files and folders will gradually keep appearing after each processing step. Here is a listing of the main files and folders divided into smaller chunks depending on the step of the analysis, in which the files/folders are generated.

1. After a successful run of the initialisation step of the pipeline, the content of the `ANALYSIS_FOLDER` will be 

      ```
      .
      ├── 00_file_sample_lane_presence
      │   ├── files
      │   ├── sample_lanes
      │   └── samples
      ├── 00_file_sample_lane_presence.done
      ├── 00_params.done
      └── params.sh
      ```

	with the file `00_params.done` indicating that the `params.sh` file was created successfully and the file `00_file_sample_lane_presence.done` indicating that the `00_file_sample_lane_presence` contains everything necessary for the further analysis steps.


2. After a successful run of the raw sequencing data quality control step, the following files and folders will be added to the `ANALYSIS_FOLDER`

	```
	├── 01_params_fastqc.done
	├── 01_raw_data_qc
	│   ├── fastqc				# fastqc reports for each raw fastq.gz file separately
	│   └── multiqc				# multiqc report summarising all of the raw fastqc reports
	├── 01_raw_data_qc.done
	├── benchmarks				# all the benchmark files generated in this step
	│   └── 01_raw_data_qc
	├── logs
	│   └── 01_raw_data_qc			# all the logs generated in this step
	└── params.sh				# updated
	```

	with the file `01_params_fastqc.done` indicating that the `params.sh` file was successfully updated before the data qc step and the file `01_raw_data_qc.done` indicating that the quality control step of the pipeline finished as expected.

3. After a successful run of the data trimming step, the following files and folders will be added to the `ANALYSIS_FOLDER`

	```
	├── 02_params_trimmomatic.done
	├── 02_trimmed_data
	│   ├── fastq				# trimmed fastq.gz files
	│   ├── fastqc				# fastqc reports for each trimmed fastq.gz file separately
	│   └── multiqc				# multiqc report summarising all of the trimmed fastqc reports
	├── 02_trim_raw_data_and_qc_trimmed.done
	├── benchmarks
	│   └── 02_trimmed_data			# all the benchmark files generated in this step
	├── logs
	│   └── 02_trimmed_data			# all the logs generated in this step
	└── params.sh				# updated
	```

	with the file `02_params_trimmomatic.done` indicating that the `params.sh` file was successfully updated before the trimming step and the file `02_trim_raw_data_and_qc_trimmed.done` indicating that the data trimming step of the pipeline finished as expected.

4. After a successful run of the data mapping step, the following files and folders will be added to the `ANALYSIS_FOLDER`

	```
	├── 03_mapping_to_ref_genome
	│   ├── bigwigs				# bigwig files - one for each sample
	│   ├── mapped_samples			# all the generated mapping bam files + some additional files
	│   └── ref_genome_index		# reference genome index generated with STAR
	├── 03_map_to_reference.done
	├── 03_params_mapping.done
	├── benchmarks
	│   └── 03_mapping_to_ref_genome	# all the benchmark files generated in this step
	├── logs
	│   └── 03_mapping_to_ref_genome	# all the logs generated in this step
	└── params.sh				# updated
	```

	with the file `03_params_mapping.done` indicating that the `params.sh` file was successfully updated before the mapping step and the file `03_map_to_reference.done` indicating that the mapping step finished as expected.

5. After a successful run of the read counting step, the following files and folders will be added to the `ANALYSIS_FOLDER`

	```
	├── 04_count_reads.done
	├── 04_params_readcount.done
	├── 04_read_counts
	│   ├── joined				# read counts joined by groups + joined all together
	│   └── separately			# read counts, one file for each sample
	├── benchmarks
	│   └── 04_read_counts			# all the benchmark files generated in this step
	├── logs
	│   └── 04_read_counts			# all the logs generated in this step
	└── params.sh				# updated
	```

	with the file `04_params_readcount.done` indicating that the `params.sh` file was successfully updated before the read counting step of the pipeline and the file `04_count_reads.done` indicating that the read counting step finished as expected.

6. After a successful run of the final multiqc run, the following files and folders will be added to the `ANALYSIS_FOLDER`

	```
	├── 05_multiqc				# final multiqc report
	├── 05_multiqc.done
	├── 05_params_multiqc.done
	├── benchmarks
	│   └── 05_multiqc			# all the benchmark files generated in this step
	└── logs
	    └── 05_multiqc			# all the logs generated in this step
	```

	with the file `05_params_multiqc.done` indicating that the necessary folder structure was created and the file `05_multiqc.done` indicating that the final multiqc step finished as expected.


## Limitations

At the moment, the pipeline can process Illumina pair-end sequenced reads that are organised into compressed `fastq.gz` files with conventional Illumina namings: `{sample_name}_{lane}_{read}_001.fastq.gz`. The samples which are split across multiple lanes will be automatically merged into one mapping file.

## Questions, Comments, Suggestions

Please raise an issue in the repository.

## ToDo

1. Adapt the helping scripts generating samplesheets similarly as in ATACseq and remove the `INPUT_FASTQ_FOLDER` variable from `pipeline_config.yaml`.
2. Switch from a purpose built container to BioContainers where possible, remove the old container from the repo.
3. Add generation of the summary table as one of the high-level rules.
4. Expand the reported summary table.

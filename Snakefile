workdir: config['ANALYSIS_FOLDER']
shell.executable("/bin/bash")
localrules: generate_params_file, check_file_sample_lane_presence, init, add_fastqc_params, raw_data_qc, add_trimmomatic_params, trim_raw_data_and_qc_trimmed, map_to_reference, add_mapping_parameters, count_reads, add_readcount_parameters, final_multiqc, add_multiqc_folder, join_read_counts, join_read_counts_groups

import pandas as pd

file_info = (
	pd.read_csv(config['INPUT_FASTQ_INFO_SHEET'], sep = "\t", header = 0, dtype=str, comment="#")
	.set_index(['file_name', 'base_name', 'group'], drop=False)
	.sort_index()
)

###
#
# High-level rules:
#
###

rule init:
	input:
		"00_file_sample_lane_presence/00_file_sample_lane_presence.done"
	output:
		touch("00_init.done")


rule raw_data_qc:
	input:
		"00_init.done",
		"01_raw_data_qc/multiqc.done"
	output:
		touch("01_raw_data_qc.done")


rule trim_raw_data_and_qc_trimmed:
	input:
		"01_raw_data_qc.done",
		"02_trimmed_data/multiqc.done"
	output:
		touch("02_trim_raw_data_and_qc_trimmed.done")


rule map_to_reference:
	input:
		"02_trim_raw_data_and_qc_trimmed.done",
		expand("03_mapping_to_ref_genome/bam_to_bigwig_{sample}.done", sample = file_info.loc[file_info['read'] == 'R1', 'sample_name'])
	output:
		touch("03_map_to_reference.done")


rule count_reads:
	input:
		"03_map_to_reference.done",
		"04_read_counts/join_read_counts_all.done",
		expand("04_read_counts/join_read_counts_{group}.done", group = set(file_info['group']))
	output:
		touch("04_count_reads.done")


rule final_multiqc:
	input:
		"04_count_reads.done",
		"05_multiqc/multiqc.done",
	output:
		touch("05_multiqc.done")




#
# Rules called by the init high-level rule:
#

rule generate_params_file:
	output:
		touch("00_params.done")
	shell:
		'''
		mkdir -p {config[ANALYSIS_FOLDER]}; \
		echo 'PIPELINE_FOLDER=\"'{config[PIPELINE_FOLDER]}'\"' > params.sh; \
		echo 'CONTAINERS_FOLDER=\"'{config[PIPELINE_FOLDER]}'/containers\"' >> params.sh; \
		echo 'SCRIPTS_FOLDER=\"'{config[PIPELINE_FOLDER]}'/scripts\"' >> params.sh
		'''

rule check_file_sample_lane_presence:
	input:
		"00_params.done",
		expand( config["INPUT_FASTQ_FOLDER"]+"/{file}", file=list(file_info.file_name) )
	output:
		touch( expand("00_file_sample_lane_presence/files/{base_name}.present", base_name=file_info.base_name) ),
		touch( expand("00_file_sample_lane_presence/sample_lanes/{sample}_{lane}.present", zip, sample=file_info.loc[file_info['read']=='R1', 'sample_name'], lane=file_info.loc[file_info['read']=='R1', 'lane']) ),
		touch( expand("00_file_sample_lane_presence/samples/{sample}.present", sample=set(file_info.sample_name)) ),
		touch("00_file_sample_lane_presence/00_file_sample_lane_presence.done")
	shell:
		'''
		mkdir -p 00_file_sample_lane_presence/files; \
		mkdir -p 00_file_sample_lane_presence/sample_lanes; \
		mkdir -p 00_file_sample_lane_presence/samples
		'''



#
# Rules called by the raw_data_qc high-level rule:
#

rule add_fastqc_params:
	input:
		"00_params.done"
	output:
		touch("01_params_fastqc.done")
	shell:
		'''
		mkdir -p logs/01_raw_data_qc; \
		mkdir -p benchmarks/01_raw_data_qc; \
		mkdir -p 01_raw_data_qc/fastqc; \
		mkdir -p 01_raw_data_qc/multiqc; \
		echo 'FASTQC_ADAPTERS=\"'{config[FASTQC_ADAPTERS]}'\"' >> params.sh; \
		echo 'FASTQC_CONTAMINANTS=\"'{config[FASTQC_CONTAMINANTS]}'\"' >> params.sh
		'''

rule fastqc_raw:
	input:
		fastqcParamsAdded = "01_params_fastqc.done",
		presenceCheck = lambda wildcards: "00_file_sample_lane_presence/files/"+wildcards.file+".present",
		fastq = config["INPUT_FASTQ_FOLDER"]+"/{file}.fastq.gz"
	output:
		touch("01_raw_data_qc/fastqc_{file}.done")
	benchmark:
		"benchmarks/01_raw_data_qc/fastqc_{file}.tsv"
	log:
		"logs/01_raw_data_qc/fastqc_{file}.log"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER = "01_raw_data_qc/fastqc",
		LOG_FOLDER = "logs/01_raw_data_qc"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/fastqc.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{input.fastq} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{wildcards.file} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/fastqc_{wildcards.file}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/fastqc_{wildcards.file}_snakemake.err
		'''

rule multiqc_raw:
	input:
		expand("01_raw_data_qc/fastqc_{file}.done", file=file_info.base_name)
	output:
		touch("01_raw_data_qc/multiqc.done")
	benchmark:
		"benchmarks/01_raw_data_qc/multiqc.tsv"
	log:
		"logs/01_raw_data_qc/multiqc.log"
	params:
		PARAMS_FILE = "params.sh",
		IN_FOLDER = "01_raw_data_qc/fastqc",
		OUT_FOLDER = "01_raw_data_qc/multiqc",
		LOG_FOLDER = "logs/01_raw_data_qc"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/multiqc.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.IN_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/multiqc_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/multiqc_snakemake.err
		'''



#
# Rules called by the trim_raw_data_and_qc_trimmed high-level rule:
#

rule add_trimmomatic_params:
	input:
		"01_params_fastqc.done"
	output:
		touch("02_params_trimmomatic.done")
	shell:
		'''
		mkdir -p logs/02_trimmed_data; \
		mkdir -p benchmarks/02_trimmed_data; \
		mkdir -p 02_trimmed_data/fastq; \
		mkdir -p 02_trimmed_data/fastqc; \
		mkdir -p 02_trimmed_data/multiqc; \
		echo 'TRIMMOMATIC_TRIMMERS=\"'{config[TRIMMOMATIC_TRIMMERS]}'\"' >> params.sh
		'''

rule trim_raw_fastq:
	input:
		trimmomaticParamsAdded = "02_params_trimmomatic.done",
		presenceCheck = lambda wildcards: "00_file_sample_lane_presence/sample_lanes/"+wildcards.sample_lane+".present",
		R1 = config["INPUT_FASTQ_FOLDER"]+"/{sample_lane}_R1_001.fastq.gz",
		R2 = config["INPUT_FASTQ_FOLDER"]+"/{sample_lane}_R2_001.fastq.gz"
	output:
		touch("02_trimmed_data/trimming_{sample_lane}.done")
	benchmark:
		"benchmarks/02_trimmed_data/trimming_{sample_lane}.tsv"
	log:
		"logs/02_trimmed_data/trimming_{sample_lane}.log"
	params:
		PARAMS_FILE= "params.sh",
		OUT_FOLDER = "02_trimmed_data/fastq",
		LOG_FOLDER = "logs/02_trimmed_data"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/trimming.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{wildcards.sample_lane} \
			{input.R1} \
			{input.R2} \
			{config[THREADS_TRIMMING]} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/trimming_{wildcards.sample_lane}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/trimming_{wildcards.sample_lane}_snakemake.err
		'''


rule fastqc_trimmed:
	input:
		presenceCheck = lambda wildcards: "00_file_sample_lane_presence/sample_lanes/"+wildcards.sample_lane+".present",
		doneTrimming = "02_trimmed_data/trimming_{sample_lane}.done"
	output:
		touch("02_trimmed_data/fastqc_{sample_lane}.done")
	benchmark:
		"benchmarks/02_trimmed_data/fastqc_{sample_lane}.tsv"
	log:
		"logs/02_trimmed_data/fastqc_{sample_lane}.log"
	params:
		PARAMS_FILE= "params.sh",
		IN_R1 = "02_trimmed_data/fastq/{sample_lane}_R1_001.trimmed.fastq.gz",
		IN_R2 = "02_trimmed_data/fastq/{sample_lane}_R2_001.trimmed.fastq.gz",
		OUT_FOLDER = "02_trimmed_data/fastqc",
		LOG_FOLDER = "logs/02_trimmed_data"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/fastqc.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.IN_R1} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{wildcards.sample_lane}_R1_001 \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/fastqc_{wildcards.sample_lane}_R1_001_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/fastqc_{wildcards.sample_lane}_R1_001_snakemake.err; \
		bash {config[PIPELINE_FOLDER]}/scripts/fastqc.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.IN_R2} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{wildcards.sample_lane}_R2_001 \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/fastqc_{wildcards.sample_lane}_R2_001_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/fastqc_{wildcards.sample_lane}_R2_001_snakemake.err
		'''

rule multiqc_trimmed:
	input:
		expand("02_trimmed_data/fastqc_{sample}_{lane}.done", zip, sample=file_info.loc[file_info['read']=='R1', 'sample_name'], lane=file_info.loc[file_info['read']=='R1', 'lane'])
	output:
		touch("02_trimmed_data/multiqc.done")
	benchmark:
		"benchmarks/02_trimmed_data/multiqc.tsv"
	log:
		"logs/02_trimmed_data/multiqc.log"
	params:
		PARAMS_FILE = "params.sh",
		IN_FOLDER = "02_trimmed_data/fastqc",
		OUT_FOLDER = "02_trimmed_data/multiqc",
		LOG_FOLDER = "logs/02_trimmed_data"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/multiqc.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.IN_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/multiqc_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/multiqc_snakemake.err
		'''


#
# Rules called by the map_to_reference high-level rule:
#

rule add_mapping_parameters:
	input:
		"02_params_trimmomatic.done"
	output:
		touch("03_params_mapping.done")
	shell:
		'''
		mkdir -p 03_mapping_to_ref_genome/ref_genome_index; \
		mkdir -p 03_mapping_to_ref_genome/mapped_samples; \
		mkdir -p 03_mapping_to_ref_genome/bigwigs; \
		mkdir -p logs/03_mapping_to_ref_genome; \
		mkdir -p benchmarks/03_mapping_to_ref_genome; \
		echo 'REFERENCE_GENOME_FASTA=\"'{config[REFERENCE_GENOME_FASTA]}'\"' >> params.sh; \
		echo 'REFERENCE_GENOME_ANNOTATION_GTF=\"'{config[REFERENCE_GENOME_ANNOTATION_GTF]}'\"' >> params.sh; \
		echo 'ADDITIONAL_STAR_INDEXING_OPTIONS=\"'{config[ADDITIONAL_STAR_INDEXING_OPTIONS]}'\"' >> params.sh; \
		echo 'ADDITIONAL_STAR_MAPPING_OPTIONS=\"'{config[ADDITIONAL_STAR_MAPPING_OPTIONS]}'\"' >> params.sh; \
		echo 'MIN_MAPPING_QUALITY=\"'{config[MIN_MAPPING_QUALITY]}'\"' >> params.sh; \
		echo 'SAM_FLAG_KEEP=\"'{config[SAM_FLAG_KEEP]}'\"' >> params.sh; \
		echo 'SAM_FLAG_REMOVE=\"'{config[SAM_FLAG_REMOVE]}'\"' >> params.sh; \
		echo 'BAMCOVERAGE_OPTIONS=\"'{config[BAMCOVERAGE_OPTIONS]}'\"' >> params.sh
		'''

rule index_reference_star:
	input:
		mappingParamsAdded = "03_params_mapping.done",
		referenceGenomeFasta = config["REFERENCE_GENOME_FASTA"],
		referenceGenomeAnnotationGtf = config["REFERENCE_GENOME_ANNOTATION_GTF"]
	output:
		touch("03_mapping_to_ref_genome/ref_genome_indexing.done")
	benchmark:
		"benchmarks/03_mapping_to_ref_genome/ref_genome_indexing.tsv"
	log:
		"logs/03_mapping_to_ref_genome/ref_genome_indexing.log"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER = "03_mapping_to_ref_genome/ref_genome_index",
		LOG_FOLDER = "logs/03_mapping_to_ref_genome"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/index_reference_star.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[THREADS_GENOME_INDEXING]} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/ref_genome_indexing_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/ref_genome_indexing_snakemake.err
		'''

rule map_to_reference_star_lanes_separately:
	input:
		doneStarIndex = "03_mapping_to_ref_genome/ref_genome_indexing.done",
		presenceCheck = lambda wildcards: "00_file_sample_lane_presence/sample_lanes/"+wildcards.sample_lane+".present"
	output:
		touch("03_mapping_to_ref_genome/mapping_to_reference_{sample_lane}.done")
	benchmark:
		"benchmarks/03_mapping_to_ref_genome/mapping_to_reference_{sample_lane}.tsv"
	log:
		"logs/03_mapping_to_ref_genome/mapping_to_reference_{sample_lane}.log"
	params:
		PARAMS_FILE = "params.sh",
		IN_R1 = "02_trimmed_data/fastq/{sample_lane}_R1_001.trimmed.fastq.gz",
		IN_R2 = "02_trimmed_data/fastq/{sample_lane}_R2_001.trimmed.fastq.gz",
		INDEX_FOLDER = "03_mapping_to_ref_genome/ref_genome_index",
		OUT_FOLDER = "03_mapping_to_ref_genome/mapped_samples",
		LOG_FOLDER = "logs/03_mapping_to_ref_genome"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/map_reads_to_reference_star.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.IN_R1} \
			{config[ANALYSIS_FOLDER]}/{params.IN_R2} \
			{wildcards.sample_lane} \
			{config[THREADS_GENOME_MAPPING]} \
			{config[ANALYSIS_FOLDER]}/{params.INDEX_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/mapping_to_reference_star_lanes_separately_{wildcards.sample_lane}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/mapping_to_reference_star_lanes_separately_{wildcards.sample_lane}_snakemake.err
		'''

rule add_readgroup_lanes_separately:
	input:
		presenceCheck = lambda wildcards: "00_file_sample_lane_presence/sample_lanes/"+wildcards.sample+"_L"+wildcards.lane+".present",
		doneMapping = "03_mapping_to_ref_genome/mapping_to_reference_{sample}_L{lane}.done"
	output:
		touch("03_mapping_to_ref_genome/add_readgroup_lanes_separately_{sample}_L{lane}.done")
	benchmark:
		"benchmarks/03_mapping_to_ref_genome/add_readgroup_lanes_separately_{sample}_L{lane}.tsv"
	log:
		"logs/03_mapping_to_ref_genome/add_readgroup_lanes_separately_{sample}_L{lane}.log"
	params:
		PARAMS_FILE = "params.sh",
		INSTRUMENT_ID = file_info.loc[file_info['read']=='R1'].loc[file_info['sample_name'] == "{sample}", 'instrument_id'],
		FLOWCELL_ID = file_info.loc[file_info['read']=='R1'].loc[file_info['sample_name'] == "{sample}", 'flowcell_id'],
		IN_SORTED_BAM = "03_mapping_to_ref_genome/mapped_samples/{sample}_L{lane}_Aligned.sortedByCoord.out.bam",
		IN_SORTED_BAI = "03_mapping_to_ref_genome/mapped_samples/{sample}_L{lane}_Aligned.sortedByCoord.out.bam.bai",
		OUT_SORTED_RG_BAM = "03_mapping_to_ref_genome/mapped_samples/{sample}_L{lane}_Aligned.sortedByCoord.out.rg.bam",
		OUT_SORTED_RG_BAI = "03_mapping_to_ref_genome/mapped_samples/{sample}_L{lane}_Aligned.sortedByCoord.out.rg.bam.bai",
		READ_GROUP_STR= "-r ID:"+"{params.INSTRUMENT_ID}"+".L{lane} -r PU:"+"{params.FLOWCELL_ID}"+".L{lane} -r SM:{sample} -r LB:"+config["LIBRARY"]+" -r PL:"+config["SEQ_PLATFORM"]+" -r CN:"+config["SEQ_CENTER"],
		OUT_FOLDER = "03_mapping_to_ref_genome/mapped_samples",
		LOG_FOLDER = "logs/03_mapping_to_ref_genome"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/add_readgroup.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.IN_SORTED_BAM} \
			\"{params.READ_GROUP_STR}\" \
			{config[THREADS_GENOME_MAPPING]} \
			{wildcards.sample}_L{wildcards.lane} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/add_readgroup_lanes_separately_{wildcards.sample}_L{wildcards.lane}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/add_readgroup_lanes_separately_{wildcards.sample}_L{wildcards.lane}_snakemake.err
		'''

rule postprocess_mapping_files_merge_lanes:
	input:
		presenceCheck = lambda wildcards: "00_file_sample_lane_presence/samples/"+wildcards.sample+".present",
		doneReadGroups = lambda wildcards: expand("03_mapping_to_ref_genome/add_readgroup_lanes_separately_{{sample}}_{lane}.done", sample=wildcards.sample, lane=file_info.loc[file_info['read']=='R1'].loc[file_info['sample_name'] == wildcards.sample, 'lane'] )
	output:
		touch("03_mapping_to_ref_genome/postprocess_mapping_files_merge_lanes_{sample}.done")
	log:
		"logs/03_mapping_to_ref_genome/postprocess_mapping_files_merge_lanes_{sample}.log"
	benchmark:
		"benchmarks/03_mapping_to_ref_genome/postprocess_mapping_files_merge_lanes_{sample}.tsv"
	params:
		PARAMS_FILE= "params.sh",
		INPUT_MERGE = lambda wildcards: expand(config["ANALYSIS_FOLDER"]+"/03_mapping_to_ref_genome/mapped_samples/"+wildcards.sample+"_{lane}_Aligned.sortedByCoord.out.rg.bam", lane=file_info.loc[file_info['read']=='R1'].loc[file_info['sample_name']== wildcards.sample, 'lane'] ),
		OUT_FOLDER = "03_mapping_to_ref_genome/mapped_samples",
		LOG_FOLDER = "logs/03_mapping_to_ref_genome"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/postprocess_mapping_files_multiple_lanes.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			\"{params.INPUT_MERGE}\" \
			{wildcards.sample} \
			{config[THREADS_GENOME_MAPPING]} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/postprocess_mapping_files_{wildcards.sample}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/postprocess_mapping_files_{wildcards.sample}_snakemake.err
		'''

rule bam_to_bigwig:
	input:
		presenceCheck = lambda wildcards: "00_file_sample_lane_presence/samples/"+wildcards.sample+".present",
		doneMappingPostprocess = "03_mapping_to_ref_genome/postprocess_mapping_files_merge_lanes_{sample}.done"
	output:
		touch("03_mapping_to_ref_genome/bam_to_bigwig_{sample}.done")
	log:
		"logs/03_mapping_to_ref_genome/bam_to_bigwig_{sample}.log"
	benchmark:
		"benchmarks/03_mapping_to_ref_genome/bam_to_bigwig_{sample}.tsv"
	params:
		PARAMS_FILE = "params.sh",
		IN_BAM = "03_mapping_to_ref_genome/mapped_samples/{sample}.final.bam",
		IN_BAI = "03_mapping_to_ref_genome/mapped_samples/{sample}.final.bam.bai",
		OUT_BIGWIG = "03_mapping_to_ref_genome/bigwigs/{sample}.final.bigWig",
		OUT_FOLDER = "03_mapping_to_ref_genome/bigwigs",
		LOG_FOLDER = "logs/03_mapping_to_ref_genome",
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/bam_to_bigwig.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.IN_BAM} \
			{wildcards.sample} \
			{config[THREADS_GENOME_MAPPING]} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_BIGWIG} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/bam_to_bigwig_{wildcards.sample}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/bam_to_bigwig_{wildcards.sample}_snakemake.err
		'''



#
# Rules called by the count_reads high-level rule:
#

rule add_readcount_parameters:
	input:
		"03_params_mapping.done"
	output:
		touch("04_params_readcount.done")
	shell:
		'''
		mkdir -p 04_read_counts/separately; \
		mkdir -p 04_read_counts/joined; \
		mkdir -p logs/04_read_counts; \
		mkdir -p benchmarks/04_read_counts; \
		echo 'FEATURECOUNTS_ADDITIONAL_OPTIONS=\"'{config[FEATURECOUNTS_ADDITIONAL_OPTIONS]}'\"' >> params.sh
		'''

rule count_reads_separately:
	input:
		readcountParamsAdded = "04_params_readcount.done",
		presenceCheck = lambda wildcards: "00_file_sample_lane_presence/samples/"+wildcards.sample+".present",
	output:
		touch("04_read_counts/count_reads_separately_{sample}.done")
	benchmark:
		"benchmarks/04_read_counts/count_reads_separately_{sample}.tsv"
	log:
		"logs/04_read_counts/count_reads_separately_{sample}.log"
	params:
		PARAMS_FILE = "params.sh",
		IN_BAM = "03_mapping_to_ref_genome/mapped_samples/{sample}.final.bam",
		OUT_FOLDER = "04_read_counts/separately",
		LOG_FOLDER = "logs/04_read_counts"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/count_reads.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]}/{params.IN_BAM} \
			{wildcards.sample} \
			{config[THREADS_READ_COUNTING]} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/count_reads_{wildcards.sample}_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/count_reads_{wildcards.sample}_snakemake.err
		'''

rule join_read_counts_groups:
	input:
		GROUP = lambda wildcards: expand("04_read_counts/count_reads_separately_{sample}.done", sample = set(file_info.loc[file_info['group'] == wildcards.group, 'sample_name']))
	output:
		touch("04_read_counts/join_read_counts_{group}.done")
	benchmark:
		"benchmarks/04_read_counts/join_read_counts_{group}.tsv"
	log:
		"logs/04_read_counts/join_read_counts_{group}.log"
	params:
		OUT_FOLDER = "04_read_counts/joined",
		LOG_FOLDER = "logs/04_read_counts",
		INPUT_FILES = lambda wildcards: expand("-i " + config["ANALYSIS_FOLDER"] + "/04_read_counts/separately/{sample}.readCounts", sample = set(file_info.loc[file_info['group'] == wildcards.group, 'sample_name']))
	shell:
		'''
		python3 {config[PIPELINE_FOLDER]}/scripts/join_readCounts.py \
		-c 6 {params.INPUT_FILES} \
		> {config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER}/joined_read_counts_{wildcards.group}.csv \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/join_read_counts_{wildcards.group}_snakemake.err
		'''

rule join_read_counts:
	input:
		expand("04_read_counts/count_reads_separately_{sample}.done", sample = set(file_info['sample_name']))
	output:
		touch("04_read_counts/join_read_counts_all.done")
	benchmark:
		"benchmarks/04_read_counts/join_read_counts_all.tsv"
	log:
		"logs/04_read_counts/join_read_counts_all.log"
	params:
		OUT_FOLDER = "04_read_counts/joined",
		LOG_FOLDER = "logs/04_read_counts",
		INPUT_FILES = expand("-i " + config["ANALYSIS_FOLDER"] + "/04_read_counts/separately/{sample}.readCounts", sample = set(file_info['sample_name']))
	shell:
		'''
		python3 {config[PIPELINE_FOLDER]}/scripts/join_readCounts.py \
		-c 6 {params.INPUT_FILES} \
		> {config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER}/joined_read_counts_all.csv \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/join_read_counts_all_snakemake.err
		'''



#
# Rules called by the final_multiqc high-level rule:
#

rule add_multiqc_folder:
	input:
		"04_params_readcount.done"
	output:
		touch("05_params_multiqc.done")
	shell:
		'''
		mkdir -p "05_multiqc"; \
		mkdir -p "logs/05_multiqc"; \
		mkdir -p "benchmarks/05_multiqc"
		'''


rule multiqc_all:
	input:
		"05_params_multiqc.done"
	output:
		touch("05_multiqc/multiqc.done")
	benchmark:
		"benchmarks/05_multiqc/multiqc.tsv"
	log:
		"logs/05_multiqc/multiqc.log"
	params:
		PARAMS_FILE = "params.sh",
		OUT_FOLDER = "05_multiqc",
		LOG_FOLDER = "logs/05_multiqc"
	shell:
		'''
		bash {config[PIPELINE_FOLDER]}/scripts/multiqc.sh \
			{config[ANALYSIS_FOLDER]}/{params.PARAMS_FILE} \
			{config[ANALYSIS_FOLDER]} \
			{config[ANALYSIS_FOLDER]}/{params.OUT_FOLDER} \
			{config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER} \
		> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/multiqc_snakemake.out \
		2> {config[ANALYSIS_FOLDER]}/{params.LOG_FOLDER}/multiqc_snakemake.err
		'''

Singularity v3.5.2 was used to build the `bioinformatics_tools.sif` container. The bioinformatics tools included in the container and their corresponding versions are listed below.

<center>

| Software | Version | DOI | URL |
| :--- | :---: | :--- | :--- |
| `fastqc` | 0.11.9 | NA | https://www.bioinformatics.babraham.ac.uk/projects/fastqc |
| `multiqc` | 1.11 | [10.1093/bioinformatics/btw354](http://dx.doi.org/10.1093/bioinformatics/btw353)  | https://multiqc.info |
| `trimmomatic` | 0.39 | [10.1093/bioinformatics/btu170](https://doi.org/10.1093/bioinformatics/btu170) | http://www.usadellab.org/cms/?page=trimmomatic |
| `STAR` | 2.7.9a | [10.1093/bioinformatics/bts635](https://doi.org/10.1093/bioinformatics/bts635) | https://github.com/alexdobin/STAR |
| `samtools` | 1.13 | [10.1093/gigascience/giab008](https://doi.org/10.1093/gigascience/giab008) | https://www.htslib.org/doc/samtools.html |
| `deeptools` | 3.5.1 | [10.1093/nar/gkw257](http://doi.org/10.1093/nar/gkw257) | https://deeptools.readthedocs.io/en/develop/index.html |
| `subread` | 2.0.1 | [ 10.1093/bioinformatics/btt656 ](https://doi.org/10.1093/bioinformatics/btt656) | http://manpages.org/featurecounts |

</center>

The container can be built from the definition file using the following command:

```
sudo singularity build \
	bioinformatics_tools.sif \
	bioinformatics_tools.def \
	&> bioinformatics_tools.build.log
```

---




# parameters mandatory for the pipeline initialisation and any further processing steps

PIPELINE_CONFIG: <absolute_path_to_this_snakemake_config_file>
CLUSTER_CONFIG: <absolute_path_to_the_cluster_config_file>

ANALYSIS_FOLDER: <absolute_path_to_the_already_existing_folder_where_the_analysis_outputs_will_be_stored>
PIPELINE_FOLDER: <absolute_path_to_the_folder_containing_this_repository>

INPUT_FASTQ_FOLDER: <absolute_path_to_the_folder_containing_all_fastq_files_or_symlinks_to_them>

INPUT_FASTQ_INFO_SHEET: <absolute_path_to_the_file_containing_info_about_all_the_samples_that_will_be_analysed>
# the file is expected to contain tab-separated values (.tsv file) with the first line of the file being its header
# and containing the following collumn names: file_name, sample_name, base_name, lane, read, group, instrument_id, flowcell_id.
# an example of the file can be found in file_info_example.tsv




# parameters for ithe data_qc processing step

# fastqc custom input data, absolute paths or empty
# # (when empty, default fastqc lists are used)
# FASTQC_ADAPTERS: "" #example of en empty string
FASTQC_ADAPTERS: <absolute_path_to_the_adapters_file_or_empty_string>
FASTQC_CONTAMINANTS: <absolute_path_to_the_contaminants_file_or_empty_string>

TRIMMOMATIC_TRIMMERS: <trimming_options>
# TRIMMOMATIC_TRIMMERS: ILLUMINACLIP:<absolute_path_to_file_with_sequences_that_should_be_trimmed>:1:10:7:7:true SLIDINGWINDOW:10:25 MINLEN:30
# TRIMMOMATIC_TRIMMERS: SLIDINGWINDOW:10:25 MINLEN:30
# more details: http://www.usadellab.org/cms/?page=trimmomatic




# parameters for the read mapping processing step

# used data resources
REFERENCE_GENOME_FASTA: <absolute_path_to_the_reference_genome_comprised_in_a_single_fasta_file>
REFERENCE_GENOME_ANNOTATION_GTF: <absolute_path_to_the_annotation_of_the_reference_genome_one_gtf_file> 


# various mapping parameters

ADDITIONAL_STAR_INDEXING_OPTIONS: <additional_indexing_options>
# options used for 2x75bp paired end sequencing: 
# --sjdbOverhang 74
# for more defails, check STAR (v2.7.9a) manual: https://github.com/alexdobin/STAR/blob/master/doc/STARmanual.pdf

ADDITIONAL_STAR_MAPPING_OPTIONS: <additional_mapping_options>
# encode standard options for mapping are like this:
# --outFilterType BySJout --outFilterMultimapNmax 20 --alignSJoverhangMin 8 --alignSJDBoverhangMin 1 --outFilterMismatchNmax 999 --outFilterMismatchNoverReadLmax 0.04 --alignIntronMin 20 --alignIntronMax 1000000 --alignMatesGapMax 10000000
# for more defails, check STAR (v2.7.9a) manual: https://github.com/alexdobin/STAR/blob/master/doc/STARmanual.pdf

# information that will be included in a bam file read group info
INSTRUMENT_ID: <ID_of_the_used_sequencing_instrument>
FLOWCELL_ID: <ID_of_the_flowcell_from_which_the_data_will_be_analysed>
LIBRARY: <info_about_sequencing_library>
SEQ_PLATFORM: <sequencing_platform,e.g.ILLUMINA>
SEQ_CENTER: <name_of_sequencing_center>

BAMCOVERAGE_OPTIONS: <additional_bamCoverage_options>
# for example:
# --effectiveGenomeSize 2913022398 --normalizeUsing RPKM --ignoreForNormalization chrX chrY chrM --outFileFormat bigwig --binSize 10
# effective genome size stated here as suggested in deeptools manual -- hg38: 2913022398
# for more details check deeptools (v3.5.1) manual: https://deeptools.readthedocs.io/en/latest/content/tools/bamCoverage.html 


MIN_MAPPING_QUALITY: 10
SAM_FLAG_KEEP: 2
SAM_FLAG_REMOVE: 1548
# to check sam flags, see: https://samtools.github.io/hts-specs/SAMv1.pdf


# parameters for read counting
FEATURECOUNTS_ADDITIONAL_OPTIONS: -p -t exon -g gene_id -O -s 0
# for possible options of the featureCounts (subread v2.0.1), see: http://manpages.org/featurecounts


# cluster performance parameters
THREADS_TRIMMING:         <int> # 3
THREADS_GENOME_INDEXING:  <int> #16
THREADS_GENOME_MAPPING:   <int> #16
THREADS_READ_COUNTING:    <int> # 4

...

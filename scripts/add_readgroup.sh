#!/bin/bash

PARAMS_FILE="$1"
IN_BAM="$2"
READ_GROUP_STR="$3"
THREADS="$4"
SAMPLE_LANE="$5"
OUT_FOLDER="$6"
LOG_FOLDER="$7"

source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/add_readgroup_${SAMPLE_LANE}_singularity.log"
WDIR="${LOG_FOLDER}/add_readgroup_${SAMPLE_LANE}_WD"
mkdir -p "${WDIR}"

set -e

OUT_BAM="${OUT_FOLDER}/${SAMPLE_LANE}_Aligned.sortedByCoord.out.rg.bam"

singularity exec \
	--cleanenv \
	-B ${IN_BAM} \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		samtools addreplacerg ${READ_GROUP_STR} -m overwrite_all \
		-@ ${THREADS} \
		${IN_BAM} \
		2> ${LOG_SINGULARITY} \
			| singularity exec \
				--cleanenv \
				-B ${IN_BAM} \
				-B ${OUT_FOLDER} \
				-W ${WDIR} \
				${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
					samtools view -b -o ${OUT_BAM} \
					-@ ${THREADS} \
					- \
					>> ${LOG_SINGULARITY} 2>&1

singularity exec \
	--cleanenv \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		samtools index -b \
		-@ ${THREADS} \
		${OUT_BAM} \
		>> ${LOG_SINGULARITY} 2>&1

rm ${IN_BAM}
rm ${IN_BAM}.bai


if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
else
	rm -r ${WDIR}
fi

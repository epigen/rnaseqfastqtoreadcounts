#!/bin/bash

PARAMS_FILE="$1"
IN_BAM="$2"
SAMPLE="$3"
THREADS="$4"
OUT_BIGWIG="$5"
OUT_FOLDER="$6"
LOG_FOLDER="$7"

source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/bam_to_bigwig_${SAMPLE}_singularity.log"
WDIR="${LOG_FOLDER}/bam_to_bigwig_${SAMPLE}_WD"
mkdir -p "${WDIR}"

set -e


singularity exec \
	--cleanenv \
	-B ${IN_BAM} \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		bamCoverage ${BAMCOVERAGE_OPTIONS} --numberOfProcessors ${THREADS} --bam ${IN_BAM} -o ${OUT_BIGWIG} \
			> ${LOG_SINGULARITY} \
			2>&1


if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
else
	rm -r ${WDIR}
fi

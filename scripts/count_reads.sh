#!/bin/bash

PARAMS_FILE="$1"
IN_BAM="$2"
SAMPLE="$3"
THREADS="$4"
OUT_FOLDER="$5"
LOG_FOLDER="$6"

source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/count_reads_${SAMPLE}_singularity.log"
WDIR="${LOG_FOLDER}/count_reads_${SAMPLE}_WD"
mkdir -p "${WDIR}"

set -e


singularity exec \
	--cleanenv \
	-B ${IN_BAM} \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		featureCounts ${FEATURECOUNTS_ADDITIONAL_OPTIONS} \
			-T ${THREADS} \
			-a ${REFERENCE_GENOME_ANNOTATION_GTF} \
			-o ${OUT_FOLDER}/${SAMPLE}.readCounts \
			${IN_BAM} \
		> ${LOG_SINGULARITY} \
		2>&1


if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
else
	rm -r ${WDIR}
fi

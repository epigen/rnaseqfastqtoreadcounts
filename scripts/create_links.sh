#!/bin/bash

# The script iterates through all subfolders of FOLDER searching for available fastq.gz files.
# It creates a subfolder FOLDER/fastq_links containing soft links to all the localised fastq.gz files.

FOLDER=$1

mkdir ${FOLDER}/fastq_links
CURRENT_LOCATION=$(pwd)
cd ${FOLDER}


for f in `ls ${FOLDER}/*/*.fastq.gz`
do
	echo $f
	g=$(basename $f)
	folders=${f%/*fastq.gz}
	sampleFolder=$(basename $folders)

	cd fastq_links
	ln -s ../${sampleFolder}/$g $g
	cd ..
done



cd ${CURRENT_LOCATION}

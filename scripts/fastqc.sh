#!/bin/bash

PARAMS_FILE="$1"
IN_FASTQ="$2"
OUT_FOLDER="$3"
FILE="$4"
LOG_FOLDER="$5"

source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/fastqc_${FILE}_singularity.log"
WDIR="${LOG_FOLDER}/fastqc_${FILE}_WD"
mkdir -p "${WDIR}"

set -e

OPTIONS=""

if [[ "${FASTQC_ADAPTERS}" != "" ]]
then
	OPTIONS="${OPTIONS} --adapters ${FASTQC_ADAPTERS}"
fi

if [[ "${FASTQC_CONTAMINANTS}" != "" ]]
then
	OPTIONS="${OPTIONS} --contaminants ${FASTQC_CONTAMINANTS}"
fi


singularity exec \
	--cleanenv \
	-B ${IN_FASTQ} \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		fastqc \
			${OPTIONS} \
			-o ${OUT_FOLDER} \
			${IN_FASTQ} \
			> ${LOG_SINGULARITY} \
			2>&1

if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
else
	rm -r ${WDIR}
fi

#!/bin/bash

FOLDER_LINKS="$1"

echo -e "file_name\tsample_name\tbase_name\tlane\tread\tgroup\tinstrument_id\tflowcell_id"

CURRENT_FOLDER=$(pwd)

cd ${FOLDER_LINKS}

for l in `ls -1 ${FOLDER_LINKS}`
do

	file_name=$(echo $l | cut -d "." -f 1 | awk 'BEGIN{FS="_"}{print $0".fastq.gz"}')
	sample_name=$(echo $l | cut -d "." -f 1 | awk 'BEGIN{FS="_"}{print $1"_"$2}')
	base_name=$(echo $l | cut -d "." -f 1 | awk 'BEGIN{FS="_"}{print $0}')
	lane=$(echo $l | cut -d "." -f 1 | awk 'BEGIN{FS="_"}{print $3}')
	read_group=$(echo $l | cut -d "." -f 1 | awk 'BEGIN{FS="_"}{print $4}')
	group="XXX"
	instrument_id=$(zcat $l | head -1 | awk 'BEGIN{FS=":"}{print substr($1,2)}')
	flowcell_id=$(zcat $l | head -1 | awk 'BEGIN{FS=":"}{print $3}')

	
	echo -e ${file_name}"\t"${sample_name}"\t"${base_name}"\t"${lane}"\t"${read_group}"\t"${group}"\t"${instrument_id}"\t"${flowcell_id}
done

cd ${CURRENT_FOLDER}


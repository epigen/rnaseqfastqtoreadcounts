#!/bin/bash

PARAMS_FILE="$1"
OUT_FOLDER="$2"
THREADS="$3"
LOG_FOLDER="$4"

source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/index_reference_star_singularity.log"
WDIR="${LOG_FOLDER}/index_reference_star_WD"
mkdir -p "${WDIR}"

set -e

singularity exec \
	--cleanenv \
	-B ${REFERENCE_GENOME_FASTA} \
	-B ${REFERENCE_GENOME_ANNOTATION_GTF} \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		STAR \
			--runMode genomeGenerate \
			--runThreadN ${THREADS} \
			--genomeDir ${OUT_FOLDER} \
			--genomeFastaFiles ${REFERENCE_GENOME_FASTA} \
			--sjdbGTFfile ${REFERENCE_GENOME_ANNOTATION_GTF} \
			${ADDITIONAL_STAR_INDEXING_OPTIONS} \
			> ${LOG_SINGULARITY} \
			2>&1

if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
else
	rm -r ${WDIR}
fi



import os
import fnmatch
import argparse
import re

parser = argparse.ArgumentParser(description='Join multiple read count tables into one table containing gene id and read counts only. Output of this script is meant to be input for the EpiGen\'s DE and WGCNA pipeline.')
parser.add_argument('-i', '--input', action='append', help='Input file(s) to process.', required=True)
parser.add_argument('-c', '--readCountColumn', type=int, default=6, help='0-based column number for the column storing read counts - it has to be the same for all the input files.')
args = parser.parse_args()

filenames=args.input
readCountColumn=args.readCountColumn

header="\"Genes\""
for f in filenames:
	basename = f.split("/")[-1]
	basename = re.sub('\.readCounts$', '', basename)
	header += ",\""+basename+"\""
print(header)

n = len(filenames)

readCounts = [open(file, 'rt') for file in filenames]

while True:
	lines = [fh.readline() for fh in readCounts]

	if not lines[0]:
		break

	# apply to non-commented lines, skip the commented ones
	if (not re.search("^#", lines[0]) and not re.search("^Geneid", lines[0])):
		cells = [ line.rstrip().split('\t')  for line in lines]
		if (n > 0):
			gene = cells[0][0]
			out = "\""+gene+"\""
			for i in range(0,n):
				out += ","+cells[i][readCountColumn]
			print(out)

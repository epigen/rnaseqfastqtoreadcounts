#!/bin/bash

set -e

PARAMS_FILE="$1"
FORWARD_READS_GZ="$2"
REVERSE_READS_GZ="$3"
SAMPLE="$4"
THREADS="$5"
INDEX="$6"
OUT_FOLDER="$7"
LOG_FOLDER="$8"


source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/map_to_reference_star_lanes_separately_${SAMPLE}_singularity.log"
WDIR="${LOG_FOLDER}/map_to_reference_star_lanes_separately_${SAMPLE}_WD"
mkdir -p "${WDIR}"


singularity exec \
	--cleanenv \
	-B ${FORWARD_READS_GZ} \
	-B ${REVERSE_READS_GZ} \
	-B ${INDEX} \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		STAR \
			--readFilesIn ${FORWARD_READS_GZ} ${REVERSE_READS_GZ} \
			--readFilesCommand zcat \
			--genomeDir ${INDEX} \
			--runThreadN ${THREADS} \
			--outFileNamePrefix ${OUT_FOLDER}/${SAMPLE}_ \
			--outSAMtype BAM SortedByCoordinate \
			--outBAMsortingThreadN 1 \
			${ADDITIONAL_STAR_MAPPING_OPTIONS} \
			> ${LOG_SINGULARITY} \
			2>&1


singularity exec \
	--cleanenv \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		samtools index -b \
			-@ ${THREADS} \
			${OUT_FOLDER}/${SAMPLE}_Aligned.sortedByCoord.out.bam \
			>> ${LOG_SINGULARITY} \
			2>&1



unmapped1="${OUT_FOLDER}/${SAMPLE}_Unmapped.out.mate1"
if [ -f "$unmapped1" ]
then 
	gzip -c $unmapped1 > $unmapped1".gz"
	rm $unmapped1
fi


unmapped2="${OUT_FOLDER}/${SAMPLE}_Unmapped.out.mate2"
if [ -f "$unmapped2" ]
then 
	gzip -c $unmapped2 > $unmapped2".gz"
	rm $unmapped2
fi





if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
else
	rm -r ${WDIR}
fi


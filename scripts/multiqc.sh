#!/bin/bash

PARAMS_FILE="$1"
IN_FOLDER="$2"
OUT_FOLDER="$3"
LOG_FOLDER="$4"

source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/multiqc_singularity.log"
WDIR="${LOG_FOLDER}/multiqc_WD"
mkdir -p "${WDIR}"

set -e

singularity exec \
	--cleanenv \
	-B ${IN_FOLDER} \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		multiqc \
			${IN_FOLDER} \
			-o ${OUT_FOLDER} \
			&> ${LOG_SINGULARITY}

if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
else
	rm -r ${WDIR}
fi


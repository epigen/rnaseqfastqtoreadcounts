#!/bin/bash

set -e

PARAMS_FILE="$1"
IN_MERGE="$2"
SAMPLE="$3"
THREADS="$4"
OUT_FOLDER="$5"
LOG_FOLDER="$6"

source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/postprocess_mapping_files_multiple_lanes_${SAMPLE}_singularity.log"
WDIR="${LOG_FOLDER}/postprocess_mapping_file_${SAMPLE}_WD"
mkdir -p "${WDIR}"

MERGED_BAM="${OUT_FOLDER}/${SAMPLE}.merged.bam"
MERGED_BAI="${MERGED_BAM}.bai"
MERGED_FLAGSTAT="${OUT_FOLDER}/${SAMPLE}.merged.flagstat.txt"
FINAL_BAM="${OUT_FOLDER}/${SAMPLE}.final.bam"
FINAL_BAI="${FINAL_BAM}.bai"
FINAL_FLAGSTAT="${OUT_FOLDER}/${SAMPLE}.final.flagstat.txt"

singularity exec \
	--cleanenv \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		samtools merge \
			-o ${MERGED_BAM} \
			-@ ${THREADS} \
			${IN_MERGE} \
		> ${LOG_SINGULARITY} \
		2>&1

singularity exec \
	--cleanenv \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		samtools flagstat \
			-@ ${THREADS} \
			${MERGED_BAM} \
		> ${MERGED_FLAGSTAT} \
		2>> ${LOG_SINGULARITY}

singularity exec \
	--cleanenv \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		samtools index -b \
			-@ ${THREADS} \
			${MERGED_BAM} \
		>> ${LOG_SINGULARITY} \
		2>&1

singularity exec \
	--cleanenv \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		samtools view \
			-@ ${THREADS} \
			-b -h -q ${MIN_MAPPING_QUALITY} -F ${SAM_FLAG_REMOVE} -f ${SAM_FLAG_KEEP} \
			${MERGED_BAM} \
		> ${FINAL_BAM} \
		2>> ${LOG_SINGULARITY}

singularity exec \
	--cleanenv \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		samtools flagstat ${FINAL_BAM} \
		> ${FINAL_FLAGSTAT} \
		2>> ${LOG_SINGULARITY}

singularity exec \
	--cleanenv \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		samtools index -b \
			-@ ${THREADS} \
			${FINAL_BAM} \
		>> ${LOG_SINGULARITY} \
		2>&1


if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
else
	rm -r ${WDIR}
fi


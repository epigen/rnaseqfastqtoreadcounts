#!/bin/bash

PARAMS_FILE="$1"
SAMPLE_LANE="$2"
IN_FORWARD_READS="$3"
IN_REVERSE_READS="$4"
THREADS="$5"
OUT_FOLDER="$6"
LOG_FOLDER="$7"

source "${PARAMS_FILE}"

LOG_SINGULARITY="${LOG_FOLDER}/trimming_${SAMPLE_LANE}_singularity.log"
WDIR="${LOG_FOLDER}/trimming_${SAMPLE_LANE}_WD"
mkdir -p "${WDIR}"

OUT_FORWARD_TRIMMED_READS="${OUT_FOLDER}/${SAMPLE_LANE}_R1_001.trimmed.fastq.gz"
OUT_FORWARD_UNPAIRED_READS="${OUT_FOLDER}/unpaired_forward_${SAMPLE_LANE}.fastq.gz"
OUT_REVERSE_TRIMMED_READS="${OUT_FOLDER}/${SAMPLE_LANE}_R2_001.trimmed.fastq.gz"
OUT_REVERSE_UNPAIRED_READS="${OUT_FOLDER}/unpaired_reverse_${SAMPLE_LANE}.fastq.gz"

set -e

singularity exec \
	--cleanenv \
	-B ${IN_FORWARD_READS} \
	-B ${IN_REVERSE_READS} \
	-B ${OUT_FOLDER} \
	-W ${WDIR} \
	${CONTAINERS_FOLDER}/bioinformatics_tools.sif \
		trimmomatic PE -threads ${THREADS} -phred33 \
			-trimlog ${LOG_FOLDER}/${SAMPLE_LANE}_trimmomatic.trimLog\
			-summary ${LOG_FOLDER}/${SAMPLE_LANE}_trimmomatic.summaryStats\
			${IN_FORWARD_READS} \
			${IN_REVERSE_READS} \
			${OUT_FORWARD_TRIMMED_READS} \
			${OUT_FORWARD_UNPAIRED_READS} \
			${OUT_REVERSE_TRIMMED_READS} \
			${OUT_REVERSE_UNPAIRED_READS} \
			${TRIMMOMATIC_TRIMMERS} \
			&> ${LOG_SINGULARITY}


gzip -c ${LOG_FOLDER}/${SAMPLE_LANE}_trimmomatic.trimLog > ${LOG_FOLDER}/${SAMPLE_LANE}_trimmomatic.trimLog.gz
rm ${LOG_FOLDER}/${SAMPLE_LANE}_trimmomatic.trimLog

if [ "$(ls -A ${WDIR})" ]
then
	echo "Warning: Singularity working directory ${WDIR} is not empty."
else
	rm -r ${WDIR}
fi
